Notepad++ GO-Highlighter (dark theme)
=======================================

Highlights GO syntax code in Notepad++.

![Screenshot](https://bitbucket.org/mariuszz/notepad-go-lang-highlighting/raw/3c6d0fa258f3849844b81f69037322d94415a87d/2013-03-17_01-20-55_notepad_plus_plus_go_syntax_highlight.png)

Required 
--------

Dark (main) theme provided by Chris Kempson's [tomorrow_night](https://github.com/chriskempson/Tomorrow-Theme)
Note: usually installed by default, just go to [Settings]>[Style Configurator]>Theme:tomorrow_night

Installation
------------

Clone and then go [View]->[User-Defined Dialogue]->[Import]->(Adapt)->[Save As]
Open a .go file and change [Language].

Happy forking :)